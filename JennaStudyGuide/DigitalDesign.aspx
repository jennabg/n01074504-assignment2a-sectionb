﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/StudyMaster.master" AutoEventWireup="true" CodeBehind="DigitalDesign.aspx.cs" %>


   <asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="Title">
        <h2> Digital Design </h2>
        </div>
    </asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    
   
    <div class="row">
   
        <div class="col-md-12">
            <h3> Floats </h3>
            <p> Today we are talking about floats. In CSS a float allows you to do exactly what it
            sounds like, float an element from your layout to either the right or left of it's container.
            Using a float allows other inline elements to wrap around the item. </p>
        </div>
        
         <div class="col-md-6">
            <h3> Floating a side bar.</h3>
            <p> Here is some code I wrote to make the sidebar section of my website float left.</p>
            <pre>
                <code>
   #side-bar {
   width:25%;
   float: right;
  }
       </code>
            </pre>
        </div>
        
          <div class="col-md-6">
            <h3> Floating multiple items </h3>
         <p> Here is some code from W3Schools that shows you how to float multiple items. </p>

            <pre>
                <code>
  * {
    box-sizing: border-box;
    }

    .box {
    float: left;
    width: 33.33%; /* three boxes (use 25% for four, and 50% for two, etc) */
    padding: 50px; /* if you want space between the images */
      }
                </code>
            </pre>
            
        </div>
        
        
    </div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="links" runat="server">
    <h4> Here are some helpful links to learn more! </h4>
    <ul class="sidebar">
        <li><a href="https://www.w3schools.com/css/css_float.asp"> W3 Schools</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/float"> Mozilla Developer</a></li>
        <li> <a href="https://css-tricks.com/all-about-floats/">CSS-Tricks</a></li>
        
    </ul>
</asp:Content>
