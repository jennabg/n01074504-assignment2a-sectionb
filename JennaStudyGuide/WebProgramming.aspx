﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/StudyMaster.master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" %>




    <asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="Title">
        <h2> JavaScript </h2>
        </div>
    </asp:Content>
      
    <asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
   
        <div class="col-md-12">
            <h3> Loops </h3>
            <p> Today we are looking at loops in JavaScript. The two loops we have studied are
            for loops and while loops. The difference is that a for loop is used when you know the 
            number of times that you want the loop to run. A while loop however continues running until
            a certain condition is met, meaning it does not have a defined number of times that it "loops"
            like a for loop.</p>
        </div>
        
         <div class="col-md-6">
            <h3> A For Loop </h3>
            <p> Here is an example of a for loop I wrote. The loop will run 10 times and display the numbers
            1-10 in the console log.</p>
             <div class="codeSnippet">
            <pre>
                <code>
  for(var i=0; i<10; i++){
    console.log(i+1);
  }
                </code>
            </pre>
                </div>
        </div>
        
          <div class="col-md-6">
            <h3> A While Loop </h3>
         <p> Here is a while loop that Sean Doyle wrote for an assignment in our Web Programming wrote. The loop looks for
            a confirmation from the user.</p>

            <div class="codeSnippet">
            <pre>
                <code>
  var checkFlag = false;
   while(checkFlag === false){
    var askUser = confirm("Are you sure?");
    if(askUser === true){
     alert("Oh, good.");
     checkFlag = true;
   }
  }
  </code> </pre>
                </div>
            
        </div>
        
        
    </div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="links" runat="server">
    <h4> Here are some helpful links to learn more! </h4>
    <ul class="sidebar">
        <li><a href="https://www.w3schools.com/js/"> W3 Schools</a></li>
        <li><a href="https://javascript.info/"> Javascript.info</a></li>
        <li> <a href="https://www.tutorialspoint.com/javascript/">Tutuorials Point</a></li>
        
    </ul>
</asp:Content>
