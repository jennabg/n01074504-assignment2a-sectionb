﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/StudyMaster.master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" %>

   <asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="Title">
        <h2> Database </h2>
        </div>
    </asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="row">
   
        <div class="col-md-12">
            <h3> Aggregate Functions </h3>
            <p> Today we are looking at aggregate functions in SQL. Aggregate functions allow
            you to perform a calculation on a set of values from your database to return a 
            singular value. Some commonly used functions are AVG, SUM and COUNT.</p>
        </div>
        
         <div class="col-md-6">
            <h3> SUM </h3>
            <p> Here is an example of some code that preforms a query including the SUM function </p>
            <pre>
                <code>
  SELECT vendor_name, SUM(payment_total) AS Invoice_Sum
  FROM vendors v JOIN invoices i
  ON v.vendor_id = i.vendor_id
  GROUP BY vendor_name
  ORDER BY Invoice_Sum DESC
                </code>
            </pre>
        </div>
        
          <div class="col-md-6">
            <h3> COUNT </h3>
         <p> Here is an example of a query including the COUNT function written by Simon Borer from our
            Database class.</p>

            <pre>
                <code>
  SELECT vendor_id AS "Vendor ID", 
   COUNT(invoice_id) AS "Invoices"
  FROM vendors
   JOIN invoices
     USING(vendor_id)
  GROUP BY vendor_id
  HAVING COUNT(invoice_id) > 2
  ORDER BY "Invoices" DESC
                </code>
            </pre>
            
        </div>
        
        
    </div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="links" runat="server">
    <h4> Here are some helpful links to learn more! </h4>
    <ul class="sidebar">
        <li><a href="https://www.codecademy.com/learn/learn-sql"> Code Academy</a></li>
        <li><a href="https://www.w3schools.com/sql/"> W3Schools</a></li>
        <li> <a href="https://www.khanacademy.org/computing/computer-programming/sql">Khan Academy</a></li>
        
    </ul>
</asp:Content>
